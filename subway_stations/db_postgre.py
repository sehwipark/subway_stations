"""PostgreSQL interface for the app.
"""
import os
import re
import csv
import datetime
from functools import lru_cache

import click
import geojson
import psycopg2
from flask import g, current_app
from flask.cli import with_appcontext

COMMIT_SIZE = 100
DB_STATION = 'stations'

HOST = os.getenv('RDS_HOSTNAME', 'localhost')
PORT = os.getenv('RDS_PORT', 5432)
USER = os.getenv('RDS_USERNAME', 'posgres')
PASSWORD = os.getenv('RDS_PASSWORD', '')


def get_db():
    """Get the global database connection. Create one if not existing.

    Returns:
        (psycopg2.Connection) A database connection
    """
    db = getattr(g, 'database', None)
    if db is None:
        db = create_connection()
        setattr(g, 'database', db)
    return db


def create_connection(
        host=HOST, port=PORT, database=USER, user=USER, password=PASSWORD):
    conn_info = locals()
    db = psycopg2.connect(**conn_info)
    return db


def get_all():
    """Get all stations data.

    Returns:
        (list of dict) All stations as dictionaries.
    """
    db = get_db()
    cur = db.cursor()
    cur.execute(f'SELECT * FROM {DB_STATION}')
    data = cur.fetchall()
    return [_make_dict(cur, row) for row in data]


def get_one(id:int):
    """Get all stations data.

    Returns:
        (dict) A station dictionary.
    """
    db = get_db()
    cur = db.cursor()
    cur.execute(f'SELECT * FROM {DB_STATION} WHERE id={id}')
    row = cur.fetchone()
    return _make_dict(cur, row)


def post(data: dict):
    """Post a new station.

    Arguments:
        data (dict): A new station dictionary.

    Returns:
        (int) The new row id
    """
    db = get_db()

    post_cols = []
    post_data = []

    for key, val in _iter_columns_from_dict(data):
        post_cols += [key]
        post_data += [val]

    cols = ', '.join(post_cols)
    vals = ', '.join(['%s'] * len(post_cols))
    sql_insert = f"""
    INSERT
        INTO {DB_STATION} ({cols})
    VALUES
        ({vals})
    RETURNING id;
    """

    cur = db.cursor()
    cur.execute(sql_insert, post_data)
    result = cur.fetchone()
    db.commit()

    return result[0]


def _iter_columns_from_dict(data: dict):
    """Pops sql columns from a geojson dictionary.

    Arguments:
        data (dict): GeoJSON style dictionary

    Yields:
        (str, any) columns and values
    """
    table_info = get_table_info()

    for col_info in table_info:
        key = col_info['name']
        dtype = col_info['type']
        val = data.get(key)
        if val is None:
            continue

        if key in ('geom', ):
            yield (key, '({0},{1})'.format(*val['coordinates']))
        else:
            yield (key, val)

    return


@lru_cache
def get_table_info():
    """
    Returns:
        (list of dict) Infomration of all columns
    """
    db = get_db()
    sql = f"""
    SELECT
        column_name as name,
        data_type as type
    FROM
        INFORMATION_SCHEMA.COLUMNS
    WHERE
        TABLE_NAME = '{DB_STATION}';
    """
    cur = db.cursor()
    cur.execute(sql)
    return [_make_dict(cur, row) for row in cur.fetchall()]


def _make_dict(cursor, row):
    """Convert a row data to a dict.
    """
    obj = {}
    for idx, val in enumerate(row):
        key = cursor.description[idx][0]
        if key in ('geom', ):
            obj[key] = geojson.Point(eval(val))
        elif isinstance(val, datetime.datetime):
            obj[key] = val.isoformat()
        else:
            obj[key] = val
    return obj


def load_sample():
    """Load sample csv data into the database.
    """
    db = get_db()

    # Load CSV
    csv_path = "samples/subway_stations.csv"
    with current_app.open_resource(csv_path, mode='r') as f:
        table = csv.reader(f, delimiter=',')
        labels = next(table)
        values = []
        for row in table:
            values += [_sample_to_sql(row)]

    # Insert into DB
    block_size = COMMIT_SIZE
    col_names = ','.join(labels)
    for left in range(0, len(values), block_size):
        sql_insert = f"""
        INSERT INTO {DB_STATION} ({col_names}) VALUES
        """
        block = values[left:left + block_size]
        sql_vals = ','.join(block)
        cur = db.cursor()
        cur.execute(sql_insert + sql_vals)
        db.commit()

    return


def _sample_to_sql(row):
    """Convert a csv row to sql values
    """
    regex_point = re.compile(r"POINT \((-?[0-9][0-9.]*) (-?[0-9][0-9.]*)\)")

    def translate_cols(row):
        for val in row:
            if isinstance(val, str):
                point = regex_point.search(val)
                if point:
                    yield "'({},{})'".format(*point.groups())
                else:
                    yield "'{}'".format(re.sub("'", "", val))
            else:
                yield f'{val}'

    return '({})'.format(','.join(translate_cols(row)))


def close_db(e=None):
    """If this request connected to the database, close the connection.
    """
    db = getattr(g, 'database', None)
    if db is not None:
        db.close()
    return


def init_db():
    """Clear existing data and create new tables.
    """
    db = get_db()
    with current_app.open_resource("schema_postgre.sql") as f:
        cur = db.cursor()
        cur.execute(f.read().decode("utf8"))
        db.commit()
    return


@click.command("load-sample")
@with_appcontext
def load_sample_command():
    """Load sample csv data into the database."""
    load_sample()
    click.echo("Loaded sample data.")


@click.command("init-db")
@with_appcontext
def init_db_command():
    """Clear existing data and create new tables."""
    init_db()
    click.echo("Initialized the database.")


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
    app.cli.add_command(load_sample_command)
