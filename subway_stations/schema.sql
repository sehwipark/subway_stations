-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS stations;

CREATE TABLE stations (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  name TEXT NOT NULL,
  latitude REAL NOT NULL,
  longitude REAL NOT NULL,
  line TEXT,
  notes TEXT
);
