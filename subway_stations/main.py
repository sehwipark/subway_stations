import os
import geojson

from flask import Flask, request, g

from subway_stations import db_sqlite, db_postgre

NAME = 'subway_stations'


def create_app():
    """Create a new Flask app and set up routes.

    Returns:
        (Flask) An app instance
    """
    app = Flask(NAME)

    @app.route("/hello")
    def hello():
        return 'Hello World.'

    @app.route("/station", methods=['GET', 'POST'])
    def all_stations():
        with app.app_context():
            if request.method == 'GET':
                data = db_postgre.get_all()
                response = app.response_class(
                    response=geojson.dumps(data),
                    status=200,
                    mimetype='application/json'
                )
                return response

            if request.method == 'POST':
                print('Request:', request.data)
                post_data = request.get_json(force=True)
                rowid = db_postgre.post(post_data)
                result = {'id': rowid, 'result': 'Success'}
                response = app.response_class(
                    response=geojson.dumps(result),
                    status=200,
                    mimetype='application/json',
                )
                return response

    @app.route("/station/<int:id>", methods=['GET'])
    def station(id):
        with app.app_context():
            data = db_postgre.get_one(id)
            response = app.response_class(
                response=geojson.dumps(data),
                status=200,
                mimetype='application/json',
            )
            return response

    return app
