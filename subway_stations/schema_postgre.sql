-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS stations;

CREATE TABLE stations (
  id SERIAL PRIMARY KEY,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  name TEXT NOT NULL,
  geom POINT NOT NULL,
  line TEXT,
  notes TEXT
);
