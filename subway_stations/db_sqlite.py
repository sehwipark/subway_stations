"""SQLite interface for the app.
"""
import re
import csv
import sqlite3
from functools import lru_cache

import geojson
import click
from flask import g, current_app
from flask.cli import with_appcontext

COMMIT_SIZE = 100
DB_STATION = 'stations'


def get_db(db_path='database.db'):
    """Get the global database connection. Create one if not existing.

    Arguments:
        db_path (str): A file path for the new database
            Default: "database.db"

    Returns:
        (sqlite3.Connection) A database connection
    """
    db = getattr(g, 'database', None)
    if db is None:
        db = sqlite3.connect(db_path)
        db.row_factory = _make_dict
        setattr(g, 'database', db)
    return db


def get_all():
    """Get all stations data.

    Returns:
        (list of dict) All stations as dictionaries.
    """
    db = get_db()
    cur = db.cursor()
    cur.execute(f'SELECT * FROM {DB_STATION}')
    data = cur.fetchall()
    return data


def get_one(id:int):
    """Get all stations data.

    Returns:
        (dict) A station dictionary.
    """
    db = get_db()
    cur = db.cursor()
    cur.execute(f'SELECT * FROM {DB_STATION} WHERE id={id}')
    return cur.fetchone()


def post(data: dict):
    """Post a new station.

    Arguments:
        data (dict): A new station dictionary.

    Returns:
        (int) The new row id
    """
    db = get_db()

    post_cols = []
    post_data = []

    for key, val in _iter_columns_from_dict(data):
        post_cols += [key]
        post_data += [val]

    cols = ', '.join(post_cols)
    vals = ', '.join('?' * len(post_cols))
    # vals = ', '.join(post_data)
    sql_insert = f"""INSERT INTO {DB_STATION} ({cols}) VALUES ({vals})"""

    cur = db.cursor()
    cur.execute(sql_insert, post_data)

    db.commit()
    return cur.lastrowid


@lru_cache
def get_table_info():
    """
    Returns:
        (list of dict) Infomration of all columns
    """
    db = get_db()
    cur  = db.cursor()
    cur.execute(f'PRAGMA table_info("{DB_STATION}");')
    return cur.fetchall()


def _iter_columns_from_dict(data: dict):
    """Pops sql columns from a geojson dictionary.

    Arguments:
        data (dict): GeoJSON style dictionary

    Yields:
        (str, any) columns and values
    """
    table_info = get_table_info()

    geom_coord = data.get('geom', {}).get('coordinates')
    if geom_coord:
        yield ('latitude', geom_coord[0])
        yield ('longitude', geom_coord[1])

    for col_info in table_info:
        key = col_info['name']
        val = data.get(key)
        if val is None:
            continue
        yield (key, val)

    return


def _make_dict(cursor, row):
    """Convert a row data to a dict.
    """
    obj = {}
    pnt = [0, 0]
    for idx, val in enumerate(row):
        key = cursor.description[idx][0]
        if key in ('latitude', ):
            pnt[0] = val
        elif key in ('longitude', ):
            pnt[1] = val
        else:
            obj[key] = val

    obj['geom'] = geojson.Point(pnt)
    return obj


def close_db(e=None):
    """If this request connected to the database, close the connection.
    """
    db = getattr(g, 'database', None)

    if db is not None:
        db.close()


def init_db():
    """Clear existing data and create new tables.
    """
    db = get_db()
    with current_app.open_resource("schema.sql") as f:
        cur = db.cursor()
        cur.executescript(f.read().decode("utf8"))


def load_sample():
    """Load sample csv data into the database.
    """
    db = get_db()

    # Load CSV
    csv_path = "samples/subway_stations.csv"
    with current_app.open_resource(csv_path, mode='r') as f:
        rows = csv.reader(f, delimiter=',')
        labels = next(rows)
        values = []
        for r in rows:
            values += [_sample_to_sql(r, labels)]

    # Insert into DB
    block_size = COMMIT_SIZE
    for left in range(0, len(values), block_size):
        cur = db.cursor()
        block = values[left:left + block_size]
        sql_insert = f"""
        INSERT INTO {DB_STATION} (id, name, latitude, longitude, line, notes)
        VALUES
        """
        sql_insert += ',\n        '.join(block)
        cur.execute(sql_insert)
        db.commit()

    return


def _sample_to_sql(row, labels):
    """Convert a csv row to sql values
    """
    regex_point = re.compile(r"POINT \((-?[0-9][0-9.]*) (-?[0-9][0-9.]*)\)")

    fields = {}
    for key, val in zip(labels, row):
        fields[key.lower()] = val

    point = regex_point.search(fields.get('geom', ''))
    fields['latitude'], fields['longitude'] = point.groups()

    sql_val = '({id}, "{name}", {latitude}, {longitude}, "{line}", "{notes}")'

    return sql_val.format(**fields)


@click.command("load-sample")
@with_appcontext
def load_sample_command():
    """Load sample csv data into the database."""
    load_sample()
    click.echo("Loaded sample data.")


@click.command("init-db")
@with_appcontext
def init_db_command():
    """Clear existing data and create new tables."""
    init_db()
    click.echo("Initialized the database.")


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
    app.cli.add_command(load_sample_command)
