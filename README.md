# Subway Stations RESTful Web Backend #

## Overview ##

A simple backend providing REST API to handles subway station information.
This app is powered by Flask web framework.

## Contents ##

1. [Configuration](#Configuration)
2. [Running a Server](#Running_a_Server)
3. [REST API](#Rest_API)

## Configuration ##

### Dependencies ###

- Flask
- GeoJSON
- psycopg2 or psycopg2-binary

### Installation ###

For Test
```console
make install
```

For Production
```console
pip install .
```

### Setup Database ###

This app requires to connect a PostgreSQL as a database.
Environment variables are required to make a remote host.

#### PostgreSQL Configuration ####

This app requires to have access to a PostgreSQL database service.
It requires environment variables below if the database is not on the localhost.

| Variable     | Description          |
| ------------ | -------------------- |
| RDS_HOSTNAME | A url for the host   |
| RDS_PORT     | A port number        |
| RDS_USERNAME | The master username |
| RDS_PASSWORD | The master password  |

Then, you can run init command to initialize the database.

```console
flask init-db
```

### Load Sample Data (Optional) ###

```console
flask load-sample
```

## Running Server ##

### Test Server ###

#### Running tests ####

This app is shipped with a set of tests.
- Please run the tests to see if your configuration is in a runnable state.
- You need to be in the python environment to fulfil package requirements.

Run tests.
```console
make test
```

To run a test server.
```console
make run
```

and check the webbroswer
```
http://localhost:5000/hello
```

### Production Server ###

Please check Flask official [documents.](https://flask.palletsprojects.com/en/2.0.x/tutorial/deploy/#run-with-a-production-server)

## REST API ##

### GET ###

All stations in GeoJSON format.
```url
http://localhost:5000/station
```

One station in GeoJSON format.
```url
http://localhost:5000/station/<id:int>
```

### POST ###
```url
http://localhost:5000/station
```

Data Format

- JSON

Required Fields

- name (Text)
- geom (Point)

Optional Fields

- line (Text)
- notes (Text)

Example
```
curl  -H "Content-Type: applicatoin/json"\
    -d'{"name":"Yaletown", "geom":{"type":"Point", "coordinates":[-123.1219, 49.2746]}}'\
    -X POST http://localhost:5000/station;
```

## Contact ##

* Sehwi Park: sehwi.dev@gmail.com

## References ##
Sample data
- NYC OpenDATA https://data.cityofnewyork.us/Transportation/Subway-Stations/arq3-7z49
