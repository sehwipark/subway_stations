NAME=subway_stations

run:
	flask run

test:
	pip install -q ".[test]"
	coverage run -m pytest -v --junitxml results.xml tests/
	@coverage html;coverage report

install:
	pip install -q ".[test]"

clean:
	pip uninstall -y ${NAME}

init:
	flask init-db

load:
	flask load-sample

connect-db:
	psql -h ${RDS_HOSTNAME} -p ${RDS_PORT} -U ${RDS_USERNAME}
