import os
import sys

import sqlite3

from flask import Flask

from subway_stations import create_app
from subway_stations import db_postgre


app = create_app()
db_postgre.init_app(app)


@app.route("/environ")
def environ():
    # for DEV
    # TODO: Delete before deployment
    data = os.environ
    txt = ''
    for key, val in data.items():
        txt += '<p>{} {}</p>'.format(key, val)
    return txt
