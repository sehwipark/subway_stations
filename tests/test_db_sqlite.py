import pytest


@pytest.fixture
def db_path():
    import tempfile
    db_dir, db_path = tempfile.mkstemp()
    return db_path


@pytest.fixture
def app(db_path):
    from subway_stations import create_app, db_sqlite
    test_app = create_app()

    db_sqlite.init_app(test_app)
    with test_app.app_context():
        # Initialize a temporary database
        _ = db_sqlite.get_db(db_path)

    return test_app


def test_get_close_db(app, db_path):
    import sqlite3
    from subway_stations.db_sqlite import get_db

    with app.app_context():
        db = get_db(db_path)
        assert db is get_db(db_path)

    with pytest.raises(sqlite3.ProgrammingError) as e:
        db.execute("SELECT 1")

    assert "closed" in str(e.value)


def test_insert_data(app, db_path):
    from subway_stations.db_sqlite import get_db, init_db
    sql_insert = """
    INSERT INTO stations (name, latitude, longitude)
    VALUES ('Astor Pl', -73.99106999861966, 40.73005400028978)
    """

    with app.app_context():
        db = get_db(db_path)
        init_db()
        cur = db.cursor()
        cur.execute(sql_insert)
        rid = cur.lastrowid

    assert rid == 1


def test_query_one(app, db_path):
    from subway_stations.db_sqlite import get_db, load_sample, init_db
    sql_query = """
    SELECT * FROM stations WHERE id=1
    """

    with app.app_context():
        db = get_db(db_path)
        init_db()
        load_sample()
        cur = db.execute(sql_query)
        result = cur.fetchall()

    assert result


def test_get_all(app, db_path):
    from subway_stations.db_sqlite import load_sample, init_db, get_all

    with app.app_context():
        init_db()
        load_sample()
        data = get_all()

    assert len(data) > 100


def test_get_one(app, db_path):
    from subway_stations.db_sqlite import load_sample, init_db, get_one

    with app.app_context():
        init_db()
        load_sample()
        data = get_one(1)

    assert isinstance(data, dict)


def test_post_one(app, db_path):
    from subway_stations.db_sqlite import load_sample, init_db, post

    new_data = {
        'name': 'Yaletown',
        'geom': {
            'type': 'Point',
            'coordinates': [-123.1219, 49.2746],
        },
        'line': 'canada',
        'notes': 'Vancouver daytime',
    }

    with app.app_context():
        init_db()
        rowid = post(new_data)

    assert isinstance(rowid, int)
