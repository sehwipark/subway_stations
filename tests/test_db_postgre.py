from flask.cli import with_appcontext
import pytest


@pytest.fixture
def app():
    from subway_stations import db_postgre, create_app
    test_app = create_app()

    db_postgre.init_app(test_app)
    with test_app.app_context():
        # Initialize a temporary database
        _ = db_postgre.get_db()

    return test_app


@pytest.fixture
def db():
    from subway_stations import db_postgre
    db = db_postgre.create_connection()
    return db


def test_get_close_db(app):
    import psycopg2
    from subway_stations.db_postgre import get_db

    with app.app_context():
        db = get_db()
        assert db is get_db()

    with pytest.raises(psycopg2.InterfaceError) as e:
        cur = db.cursor()
        cur.execute("SELECT 1")

    assert "closed" in str(e.value)


def test_init_db(app):
    from subway_stations.db_postgre import get_db, init_db
    with app.app_context():
        db = get_db()
        init_db()
    assert True


def test_get_tableinfo(app):
    from subway_stations.db_postgre import get_table_info
    with app.app_context():
        info = get_table_info()
    assert len(info) > 0


def test_load_samples(app):
    from subway_stations.db_postgre import get_db, init_db, load_sample
    with app.app_context():
        db = get_db()
        init_db()
        load_sample()
    assert True


def test_insert_data(app):
    sql_insert = """
    INSERT INTO stations (name, geom)
    VALUES ('Astor Pl', POINT(-73.99106999861966, 40.73005400028978))
    RETURNING id
    """
    from subway_stations.db_postgre import get_db, init_db, load_sample
    with app.app_context():
        db = get_db()
        init_db()

        cur = db.cursor()
        cur.execute(sql_insert)
        db.commit()

        result = cur.fetchone()
        rid = result[0]

    assert rid > 0


def test_query_one(db):
    sql_query = """
    SELECT * FROM stations WHERE id=1
    """

    cur = db.cursor()
    cur.execute(sql_query)
    result = cur.fetchone()

    assert result


def test_get_all(app):
    from subway_stations.db_postgre import load_sample, init_db, get_all, get_db

    with app.app_context():
        db = get_db()
        init_db()
        load_sample()
        data = get_all()
    assert len(data) > 100


def test_get_one(app):
    from subway_stations.db_postgre import load_sample, init_db, get_one

    with app.app_context():
        init_db()
        load_sample()
        data = get_one(1)

    assert data


def test_post_one(app):
    from subway_stations.db_postgre import load_sample, init_db, post

    new_data = {
        'name': 'Yaletown',
        'geom': {
            'type': 'Point',
            'coordinates': [-123.1219, 49.2746],
        },
        'line': 'canada',
        'notes': 'Vancouver daytime',
    }

    with app.app_context():
        init_db()
        rowid = post(new_data)

    assert isinstance(rowid, int)
