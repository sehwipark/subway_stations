import pytest


@pytest.fixture
def db_path():
    import tempfile
    _, db_path = tempfile.mkstemp()
    return db_path


@pytest.fixture
def app(db_path):
    from subway_stations import create_app, db_sqlite
    test_app = create_app()

    db_sqlite.init_app(test_app)
    with test_app.app_context():
        db = db_sqlite.get_db(db_path)
        db_sqlite.init_db()
        db_sqlite.load_sample()

    return test_app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()


def test_get_hello(app, client):
    response = client.get('/hello')
    assert b'Hello' in response.data


def test_get_all_stations(app, client):
    with app.app_context():
        # load_sample()
        response = client.get('/station')

    assert response.status_code == 200
    assert isinstance(response.json, list)


def test_get_one_stations(app, client):
    with app.app_context():
        # load_sample()
        response = client.get('/station/1')

    assert response.status_code == 200
    assert isinstance(response.json, dict)


def test_post_station(app, client, db_path):
    from subway_stations.db_sqlite import get_db, init_db

    new_data = {
        'name': 'Burrad',
        'geom': {
            'type': 'Point',
            'coordinates': [-123.1202, 49.2856],
        },
        'line': 'expo',
        'notes': 'Vancouver daytime',
    }

    response = client.post('/station', json=new_data)

    assert response.status_code == 200
    assert response.json
